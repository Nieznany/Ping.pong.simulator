#!/usr/bin/env python
from flask import Flask, render_template, session, request
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect, send
from random import random, randint

# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = None

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, async_mode=async_mode)
log = []
log2 = []
ping_skill = 0.9
pong_skill = 0.91



def background_thread(sid):
    """Wysyłanie ping pong gone"""
    start = True
    j = 0
    k = randint(0,1)
    while sid in log2:
        socketio.sleep(0.32)
        if not start:
            if (j == 0 and random() > ping_skill) or (j==1 and random() > pong_skill):
                start = True
                socketio.send('gone', room = sid)
                log.append('gone')
            else:
                if j == 0:
                    socketio.send('ping', room = sid)
                    j = 1
                    log.append('ping')
                else:
                    socketio.send('pong', room = sid)
                    j = 0
                    log.append('pong')
                
        else:
            start = False
            k = (k+1)%2
            if k == 0:
                socketio.send('ping',room = sid)
                j = 1
                log.append('ping')
            else:
                socketio.send('pong', room = sid)
                j = 0
                log.append('pong')

@app.route('/')
def index():
    return render_template('index.html', async_mode=socketio.async_mode)


@socketio.on('connect')
def test_connect():
    log2.append(request.sid)
    socketio.start_background_task(target=background_thread, sid = request.sid)

    

@socketio.on('disconnect')
def test_disconnect():
    log2.remove(request.sid)
    print('Client disconnected', request.sid)


if __name__ == '__main__':
    socketio.run(app, debug=True)
