import sys
from nose.tools import eq_, raises

sys.path.append('source')

import testowy  # NOQA


def test_text():
    eq_(testowy.text, "Hello World")


@raises(AttributeError)
def test_no_test():
    testowy.tekst
