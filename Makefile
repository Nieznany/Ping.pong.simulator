PIP = . venv/bin/activate; pip3
PIPCOMPILE = . venv/bin/activate; pip-compile
PIPSYNC = . venv/bin/activate; pip-sync
PYTHON = . venv/bin/activate; python3
IPYTHON = . venv/bin/activate; ipython
NOSETESTS = . venv/bin/activate; nosetests


default: run

venv: venv/bin/activate

venv/bin/activate: requirements.in
	test -d venv || (python -m venv venv; ${PIP} install -U pip setuptools; ${PIP} install pip-tools)
	${PIPCOMPILE} requirements.in
	${PIPSYNC}
	touch venv/bin/activate

run: venv
	${PYTHON} source/app.py

upgrade: venv
	${PIPCOMPILE} requirements.in
	${PIPSYNC}

kernel: venv
	${IPYTHON} kernel

test: venv
	${NOSETESTS} -v -s tests

check: venv
	${PIP} uninstall setuptools && ${PIP} install -U setuptools
	${PYTHON} -m flake8 source; true
	${PYTHON} -m flake8 tests; true

clean:
	find source |grep "__pycache__$$"| xargs rm -rf
	find tests |grep "__pycache__$$"| xargs rm -rf
